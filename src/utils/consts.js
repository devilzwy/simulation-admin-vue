const consts = {}
// 所有是与否的通用常量：1是 0否
consts.yesOrNoList = [
  {
    value: 1,
    label: '是'
  }, {
    value: 0,
    label: '否'
  }
]
// 投射方式 1投掷式 2射击式 3自推式 4布设式 9其他
consts.launchTypeList = [
  {
    value: 1,
    label: '投掷式'
  }, {
    value: 2,
    label: '射击式',
  }, {
    value: 3,
    label: '自推式',
  }, {
    value: 4,
    label: '布设式',
  }, {
    value: 9,
    label: '其他'
  }
]
// cep类型 1地面CEP 2弹道CEP
consts.cepTypeList = [
  {
    value: 1,
    label: '地面CEP'
  }, {
    value: 2,
    label: '弹道CEP'
  }
]
// 热处理工艺：0无 1车辆装甲型号钢 2两次淬火
consts.heatProcessList = [
  {
    value: 0,
    label: '无'
  }, {
    value: 1,
    label: '车辆装甲型号钢'
  }, {
    value: 2,
    label: '两次淬火'
  }
]
// 金属类别：1铁 2钢 3铝 4钨合金
consts.metalTypeList = [
  {
    value: 1,
    label: '铁'
  }, {
    value: 2,
    label: '钢',
  }, {
    value: 3,
    label: '铝',
  }, {
    value: 4,
    label: '钨合金'
  }
]

// 资源类型：1实物模型-装备 2实物模型-弹药 3威力模型(战斗部） 4易损性模型 5效应计算模型 6弹道模型
consts.resourceTypeList = [
  {
    value: 1,
    label: '实物模型-装备'
  }, {
    value: 2,
    label: '实物模型-弹药',
  }, {
    value: 3,
    label: '威力模型(战斗部）',
  }, {
    value: 4,
    label: '易损性模型',
  }, {
    value: 5,
    label: '效应计算模型',
  }, {
    value: 6,
    label: '弹道模型'
  }
]

// 目标类型：0楼房 1桥梁 2掩蔽库 3飞机 4车辆 5雷达站 6船
consts.accidentTypeList = [
  {
    value: 0,
    label: '楼房'
  }, {
    value: 1,
    label: '桥梁'
  }, {
    value: 2,
    label: '掩蔽库',
  }, {
    value: 3,
    label: '飞机',
  }, {
    value: 4,
    label: '车辆',
  }, {
    value: 5,
    label: '雷达站',
  }, {
    value: 6,
    label: '船'
  }
]

// 毁伤判别法：1毁伤树判别 2整体判别
consts.judgeMethodList = [
  {
    value: 1,
    label: '毁伤树判别'
  }, {
    value: 2,
    label: '整体判别'
  }
]
// 毁伤树名称：1A级(Ⅰ级)严重毁伤 2B级(Ⅱ级)中度毁伤 3C级(Ⅲ级)轻度毁伤 4D级(Ⅳ级)轻微毁伤
consts.accidentNatureList = [
  {
    value: 1,
    label: 'A级(Ⅰ级)严重毁伤'
  }, {
    value: 2,
    label: 'B级(Ⅱ级)中度毁伤'
  }, {
    value: 3,
    label: 'C级(Ⅲ级)轻度毁伤'
  }, {
    value: 4,
    label: 'D级(Ⅳ级)轻微毁伤'
  }
]

// 网络输出选项：1等效部件输出等效网络，未等效部件输出原始网络 2只输出等效网络 3只输出原始网络
consts.networkTypeList = [
  {
    value: '1',
    label: '等效部件输出等效网络，未等效部件输出原始网络'
  }, {
    value: '2',
    label: '只输出等效网络'
  }, {
    value: '3',
    label: '只输出原始网络'
  }
]

// 引信类型：1触发 2近炸
consts.fuzeTypeList = [
  {
    value: 1,
    label: '触发'
  }, {
    value: 2,
    label: '近炸'
  }
]
// 炸高类型：1在预期落点的弹道上 2在落点正上方
consts.blastHighTypeList = [
  {
    value: 1,
    label: '在预期落点的弹道上'
  }, {
    value: 2,
    label: '在落点正上方'
  }
]
// 炸高计算方法：炸高值计算方法：1输入 2正态分布抽样 3均匀分布抽样
consts.calculationMethodList = [
  {
    value: 1,
    label: '输入'
  }, {
    value: 2,
    label: '正态分布抽样'
  }, {
    value: 3,
    label: '均匀分布抽样'
  }
]
// 弹类型：1杀爆弹 2侵爆弹 3整爆弹 4半穿甲弹 5子母弹 6破甲弹
consts.warheadTypeList = [
  {
    value: 1,
    label: '杀爆弹'
  }, {
    value: 2,
    label: '侵爆弹'
  }, {
    value: 3,
    label: '整爆弹'
  }, {
    value: 4,
    label: '半穿甲弹'
  }, {
    value: 5,
    label: '子母弹'
  }, {
    value: 6,
    label: '破甲弹'
  }
]
// 布靶坐标系：1极坐标系 2直角坐标系
consts.coordinateTypeList = [
  {
    value: 1,
    label: '极坐标系'
  }, {
    value: 2,
    label: '直角坐标系'
  }
]

// 打击方式：1单弹 2多弹
consts.strikeMethodList = [
  {
    value: 1,
    label: '单弹'
  }, {
    value: 2,
    label: '多弹'
  }
]

// 目标数量：1单目标 2集群目标
consts.targetQuantityList = [
  {
    value: 1,
    label: '单目标'
  }, {
    value: 2,
    label: '集群目标'
  }
]

// 结构对称性：0非对称 1上下对称
consts.structureTypeList = [
  {
    value: 0,
    label: '非对称'
  }, {
    value: 1,
    label: '上下对称'
  }
]
// 长度单位：1毫米 2厘米 3分米 4米 5千米
consts.lengthUnitList = [
  {
    value: 1,
    label: '毫米'
  }, {
    value: 2,
    label: '厘米'
  }, {
    value: 3,
    label: '分米'
  }, {
    value: 4,
    label: '米'
  }, {
    value: 5,
    label: '千米'
  }
]
// 质量单位：1毫克 2克 3千克
consts.weightUnitList = [
  {
    value: 1,
    label: '毫克'
  }, {
    value: 2,
    label: '克'
  }, {
    value: 3,
    label: '千克'
  }
]
// 时间单位：1微秒 2毫秒 3秒 4分钟 5小时
consts.timeUnitList = [
  {
    value: 1,
    label: '微秒'
  }, {
    value: 2,
    label: '毫秒'
  }, {
    value: 3,
    label: '秒'
  }, {
    value: 4,
    label: '分钟'
  }, {
    value: 5,
    label: '小时'
  }
]
// 排布类型：1紧凑排布 2间隔排布
consts.arrangeTypeList = [
  {
    value: 1,
    label: '紧凑排布'
  }, {
    value: 2,
    label: '间隔排布'
  }
]
// 破片形状：1球形 2圆柱形 3立方形 4菱形
consts.fragmentShapeTypeList = [
  {
    value: 1,
    label: '球形'
  }, {
    value: 2,
    label: '圆柱形'
  }, {
    value: 3,
    label: '立方形'
  }, {
    value: 4,
    label: '菱形'
  }
]
// 引信类型 1触发引信 2非触发引信 3执行引信
consts.fuzeTypeList = [
  {
    value: 1,
    label: '触发引信'
  }, {
    value: 2,
    label: '非触发引信'
  }, {
    value: 3,
    label: '执行引信'
  }
]
// 层类型：1壳体 2装药 3破片 4内衬 5隔板 6端盖 7引信 8缓冲 9药型罩内层 10药型罩外层
consts.layerTypeList = [
  {
    value: 1,
    label: '壳体'
  }, {
    value: 2,
    label: '装药'
  }, {
    value: 3,
    label: '破片'
  }, {
    value: 4,
    label: '内衬'
  }, {
    value: 5,
    label: '隔板'
  }, {
    value: 6,
    label: '端盖'
  }, {
    value: 7,
    label: '引信'
  }, {
    value: 8,
    label: '缓冲'
  }, {
    value: 9,
    label: '药型罩内层'
  }, {
    value: 10,
    label: '药型罩外层'
  }
]
// 段类型：1默认段 2左端盖 3右端盖 4侵彻半部段 5引信 6药型罩段
consts.sectionTypeList = [
  {
    value: 1,
    label: '默认段'
  }, {
    value: 2,
    label: '左端盖'
  }, {
    value: 3,
    label: '右端盖'
  }, {
    value: 4,
    label: '侵彻半部段'
  }, {
    value: 5,
    label: '引信'
  }, {
    value: 6,
    label: '药型罩段'
  }
]
// 层形状：0厚度描述 1圆台 2圆柱 3卵形 4二元二次曲线 5多项式拟合曲线
consts.layerShapeTypeList = [
  {
    value: 0,
    label: '厚度描述'
  }, {
    value: 1,
    label: '圆台'
  }, {
    value: 2,
    label: '圆柱'
  }, {
    value: 3,
    label: '卵形'
  }, {
    value: 4,
    label: '二元二次曲线'
  }, {
    value: 5,
    label: '多项式拟合曲线'
  }
]
// 威力类型：1静爆威力 2动爆威力 3冲击波与准静态压力 5侵彻威力 6穿甲威力
consts.powerTypeList = [
  {
    value: 1,
    label: '静爆威力'
  }, {
    value: 2,
    label: '动爆威力'
  }, {
    value: 3,
    label: '冲击波与准静态压力'
  }, {
    value: 5,
    label: '侵彻威力'
  }, {
    value: 6,
    label: '穿甲威力'
  }
]

export default consts
